import Vue from "vue";
import Vuex from "vuex";
import formatMoney from '../helpers/money'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    requesting: false,
    success: false,
    error: false,
    currency: "USD",
    amount: '40',
    presets: ['40', '100', '200', '1000', '2500', '5000'],
    currencies: [
      { name: "US Dollar", code: "USD", symbol: "$", rate: 1 },
      { name: "Euro", code: "EUR", symbol: "€", rate: 0.897597 },
      { name: "British Pound", code: "GBP", symbol: "£", rate: 0.81755 },
      { name: "Russian Ruble", code: "RUB", symbol: "₽", rate: 63.461993 },
    ],
  },
  mutations: {
    donateRequest(state) {
      state.success = false, 
      state.error = false,
      state.requesting = true
    },
    donateSuccess(state) {
      state.success = true, 
      state.error = false,
      state.requesting = false
    },
    donateError(state) {
      state.success = false, 
      state.error = true,
      state.requesting = false
    },
    currencyChanged(state, payload) {
      if(state.currency === payload){
        return;
      }
      const currentCurrencyObj = state.currencies.find(curr => curr.code === state.currency);
      const newCurrencyObj = state.currencies.find(curr => curr.code === payload);

      if(!newCurrencyObj || !currentCurrencyObj) {
        return;
      }

      state.amount = formatMoney(state.amount+'', currentCurrencyObj.rate, newCurrencyObj.rate, payload); 

      const newPresets = state.presets.map(preset => {
        return formatMoney(preset+'', currentCurrencyObj.rate, newCurrencyObj.rate, payload)
      });

      state.presets = newPresets;
      state.currency = payload;
    },
    changeAmount(state, amount) {
      state.amount = amount
    }
  },
  actions: {
    changeAmount({commit}, amount) {
      commit('changeAmount', amount)
    },
    changeCurrency({ commit }, currency) {
        commit('currencyChanged', currency)
    },
    fetchData({ commit, state }, donation) {
      console.log(donation.amount, donation.currency)
      commit("donateRequest");
      fetch("http://localhost:3090/donate", {
        method: "POST",
        headers: {
          "Content-Type": "application/json;charset=utf-8",
        },
        body: JSON.stringify({ amount: donation.amount, currency: donation.currency }),
      })
        .then((res) => {
          if (res.ok) {
            return res.json();
          }
        })
        .then((data) => {
          console.log(data);
          if (data.ok) {
            commit("donateSuccess");
            alert('Thank you for your donation!')
          }
        }).catch(err => {
          throw(new Error('coludnt fetch'))
        });
    },
  },
  modules: {},
});