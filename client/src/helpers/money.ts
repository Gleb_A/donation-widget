const round = (num: number, precision: number) => {
  return Math.ceil(num / precision) * precision;
};

const format = (num: number | string, currency:string) => {
  if(typeof num === 'string') {
     num = parseFloat(num);
  }

  const formatter = new Intl.NumberFormat("en-US", {
    currency: currency,
  });

  return formatter.format(num);
}

const deFormat = (num: string):number => {
  return parseFloat( num.replace(',', '') );
}

const formatMoney = (
  amount: string,
  rateUS: number,
  rate: number,
  currency: string
) => {
  const converted = (deFormat(amount) / rateUS) * rate;
  return format(round(converted, 10), currency);
};

export default formatMoney;

export {
  format,
  deFormat
}
