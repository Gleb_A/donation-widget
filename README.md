# Donation widget

Форма приема пожертвований

## Установка

```
$ npm install -g typescript
$ git clone https://gitlab.com/Gleb_A/donation-widget.git
$ cd /client 
$ npm install 
$ cd /server 
$ npm install
$ tsc
```



Запуск:
---

__Клиентское приложение /client :__

```sh
$ npm run serve
```
__Сервер /server :__

```sh
$ npm run dev
```