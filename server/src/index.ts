import express, { Request, Response } from 'express';
import http from 'http';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import mongoose from 'mongoose';
import Donate from './models/donate';
import cors from 'cors';

const app: express.Application = express();

//App
app.use(morgan('combined'))
app.use(cors())
app.use(bodyParser.json({ type: '*/*' }))


//Router
app.post('/donate', (req: Request, res: Response):void => {
    const amount = +req.body.amount;
    const currency = req.body.currency.toString().toUpperCase();

    if( !validate( amount, currency ) ) {
        res.json({
            ok: false
        })
    }

    const donate = new Donate({
        amount: amount,
        currency: currency
    })

    donate.save((err) => {
        if( err ) {
            res.json({
                ok: false
            })
        }
        res.json({
            ok: true
        })
    })
})

const currencies = [
	{name: "US Dollar", code: "USD", symbol: "$", rate: 1},
	{name: "Euro", code: "EUR", symbol: "€", rate: 0.897597},
	{name: "British Pound", code: "GBP", symbol: "£", rate: 0.81755},
	{name: "Russian Ruble", code: "RUB", symbol: "₽", rate: 63.461993}
];

const validate = (amount: number, currency: string):boolean => {
    
    if( amount < 0 ) {
        return false;
    }
    if(!currency){
        return false;
    }
    const idx = currencies.findIndex( item => item.code === currency);
   
    if(idx === -1) {
        return false;
    }

    return true;
}

//DB
mongoose.connect(
    'mongodb://localhost:27017/donate', 
    { useNewUrlParser: true, useUnifiedTopology: true })

//Server
const port = process.env.port || 3090;
const server = http.createServer(app);
server.listen(port);

console.log('Server listening on:', port);