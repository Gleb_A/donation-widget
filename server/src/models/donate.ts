import mongoose, { Schema } from 'mongoose';

const DonateScheme: Schema = new Schema({
    amount: {type: Number, required: true},
    currency: {type: String, required: true, uppercase: true}
})

export default mongoose.model('Donate', DonateScheme, 'donations')